import pygame
from pygame.locals import *

pygame.init()

screen_width = 900
screen_height = 600

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Platformer')

# Defining game variables
tile_size = 200

# loading photos - remember to ACTUALLY GET photos.
sky_img = pygame.image.load('/Users/liamp/Desktop/ALL DIGITECH FILES/Platformer-assets/img/sky.png')

class World():
    def __init__(self, data):
        self.tile_list = []

        # load images

        dirt_img = pygame.image.load('/Users/liamp/Desktop/ALL DIGITECH FILES/Platformer-assets/img/dirt.png')


        row_count = 0
        for row in data:
            col_count = 0
            for tile in row:
                if tile == 1:
                    img = pygame.transform.scale(dirt_img, (tile_size, tile_size))
                    img_rect = img.get_rect()
                    img_rect.x = col_count * tile_size
                    img_rect.y = col_count * tile_size
                    tile = (img, img_rect)
                    self.tile_list.append(tile)
                col_count += 1
            row_count += 1

    def draw(self):
        for tile in self.tile_list:
            screen.blit(tile[0], tile[1])

world_data = [

[1, 1, 1, 1, 1],
[1, 0, 0, 0, 1],
[1, 0, 0, 0, 1],
[1, 0, 0, 0, 1],
[1, 1, 1, 1, 1],
]

world = World(world_data)

run = True
while run == True:

    screen.blit(sky_img, (0, 0))

    world.draw()

    print(world.tile_list)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

        pygame.display.update()
pygame.quit()


